-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2022 at 01:09 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cancerhero`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountdoctor`
--

CREATE TABLE `accountdoctor` (
  `iddoc` int(11) NOT NULL,
  `KKI` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accountdoctor`
--

INSERT INTO `accountdoctor` (`iddoc`, `KKI`, `username`, `password`) VALUES
(9, 0, 'sss', '$2y$10$uJlu1yK0W93OsPp2zYWPjOjBKvmOWByd5cRjAjd.YR66Lh/YyWxSS'),
(10, 19102051, 'doctor', '$2y$10$kBNVrU27YObG2TBt1CrqMOQvbHzKYWGyGHkpvqsJYRboHWnS/cJ1S');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `newsid` int(5) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `subtitle` varchar(4200) DEFAULT NULL,
  `image` varchar(1000) CHARACTER SET latin1 DEFAULT NULL,
  `link` varchar(60) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`newsid`, `title`, `subtitle`, `image`, `link`, `date`) VALUES
(1, 'Bersyukur Bisa Jalani Kemoterapi Keempat, Ari Lasso: Kebaikan Tuhan Tiada Batasnya', 'bersyukur bisa berjalan', 'Untitled.png', 'https://www.youtube.com/watch?v=PiAFVpX7Q94&ab_channel=Warta', '2022-01-24'),
(2, 'Insiden Terus Meningkat: Waspada dan Kenali Kanker Paru NSCLC EGFR (-)', 'Jakarta, 26 November 2021 – Dalam rangka bulan kesadaran kanker paru, Yayasan Kanker Indonesia (YKI) bekerjasama dengan perusahaan farmasi multinasional yang berfokus dalam penelitian dan pengembangan obat dan vaksin inovatif, Merck Sharp & Dohme (MSD), menyosialisasikan pentingnya kewaspadaan terhadap kanker paru bukan besar (NSCLC) EGFR (-).Kanker paru menjadi penyebab sekitar 11 persen atau 2.206.771 kasus baru kanker dan kematian akibat kanker nomor satu di dunia dan di Indonesia penyebab 8,8 persen atau 34.783 kasus baru (GLOBOCAN 2020)[i]. Dari kejadian kanker paru tersebut, lebih dari 80% merupakan tipe kanker paru Sel Bukan Kecil (non small cell lung cancer atau NSCLC)[ii] dan sekitar 40?ri NSCLC terjadi mutasi reseptor pertumbuhan epidermal (EGFR).[iii] Setelah ditemukan kanker paru, rata-rata kesintasan 5-tahunan atau prosentase pasien hidup sekurangnya lima tahun adalah sebesar 21%. Rata-rata kesintasan 5-tahunan untuk laki-laki sebesar 17%, sedangkan untuk wanita sebesar 24%. Adapun kesintasan 5-tahunan untuk NSCLC sebesar 25%, dibandingkan dengan 7% untuk kanker paru sel kecil.[iv] Ketua Umum Yayasan Kanker Indonesia, Prof. DR. dr. Aru Wisaksono Sudoyo, Sp.PD-KHOM, FINASIM, FACP mengatakan, “Sebanyak 80% pasien kanker paru datang sudah stadium lanjut, sehingga prosentase kesintasan menjadi lebih rendah.  Oleh sebab itu, sangatlah penting untuk meningkatkan pengetahuan masyarakat akan faktor risiko, gejala dan pertimbangan khusus pengobatan kanker paru. Maka, YKI berharap masyarakat melakukan pencegahan kanker dengan menerapkan pola hidup sehat, tidak merokok, dan melakukan deteksi dini kanker, sebab kanker yang ditemukan dalam stadium dini mudah diobati bahkan bisa sembuh.” George Stylianou, Managing Director MSD di Indonesia, mengatakan, “Kami senang dapat berkolaborasi dengan Yayasan Kanker Indonesia, karena tantangan kanker paru tidak dapat dihadapi oleh hanya satu pemangku kepentingan saja. Itu sebabnya kami bekerja sama dengan pemerintah, organisasi pasien, tenaga kesehatan profesional, dan akademisi untuk menemukan solusi akses terhadap obat inovatif dan peluang untuk terus meningkatkan kesadaran masyarakat tentang penyakit mengerikan ini.”“Di MSD, kami bekerja dengan urgensi untuk mengutamakan pasien dan memastikan obat kanker inovatif kami dapat diakses oleh pasien yang membutuhkan. Kita masing-masing didorong oleh visi bersama untuk memberi semua pasien kanker lebih banyak—lebih banyak cara untuk mengobati kanker mereka, lebih banyak kualitas dalam hidup mereka, lebih banyak waktu,” ujar George. Dr. Ralph Girson Ginarsa, SpPD-KHOM, spesialis penyakit dalam dan konsultan hematologi onkologi medik, menjelaskan bahwa terdapat beberapa subtipe NSCLC yaitu adenokarsinoma, karsinoma sel skuamosa, dan sel besar karsinoma. Adenokarsinoma berawal dari sel-sel yang biasanya mengeluarkan zat seperti lendir dan biasanya ditemukan pada orang yang merokok atau yang dahulu perokok, namun juga ditemukan pada orang yang tidak merokok, umumnya ditemukan pada perempuan, dan cenderung pada orang yang lebih muda dibandingkan jenis kanker paru lainnya. Jenis ini ditemukan di bagian luar paru dan kemungkinannya ditemukan sebelum menyebar. Subtipe berikutnya adalah karsinoma sel skuamosa yang dimulai dari sel-sel skuamosa yang merupakan sel datar di dalam saluran udara paru. Penderita subtipe ini sangat erat kaitannya dengan kebiasaan merokok, dan cenderung ditemukan di bagian tengah paru di dekat saluran bronkus. Sedangkan subtipe sel besar karsinoma dapat ditemukan di bagian manapun di paru dan cenderung tumbuh dan berkembang dengan cepat sehingga lebih sulit untuk diobati. Ada satu subtipe sel besar karsinoma yang dikenal dengan sel besar karsinoma neuroendokrin, yang merupakan kanker yang tumbuh pesat dan serupa dengan kanker paru sel kecil.[v]Sementara itu, reseptor faktor pertumbuhan epidermal (EGFR) merupakan protein pada sel yang membantu pertumbuhannya.  Sebuah mutasi pada gen EGFR akan menyebabkannya tumbuh berlebihan sehingga menyebabkan kanker.  Jika EGFR negatif, artinya sel tumor pada kanker paru tidak memiliki mutasi EGFR.Lebih lanjut Dr. Ralph mengatakan, “Gejala pada kanker paru NSCLC maupun jenis kanker par', 'cc.png', 'https://yayasankankerindonesia.org/article/insiden-terus-men', '2022-01-29');

-- --------------------------------------------------------

--
-- Table structure for table `thread`
--

CREATE TABLE `thread` (
  `idthread` int(3) NOT NULL,
  `title` varchar(20) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `usertype` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `thread`
--

INSERT INTO `thread` (`idthread`, `title`, `content`, `date`, `usertype`) VALUES
(53, 'hallo', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut errorLorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.', '2022-01-29', 'doctor'),
(54, 'surprise', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.', '2022-01-29', 'doctor'),
(55, 'third', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisnsectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui. consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit.andae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ', '2022-01-29', 'doctor'),
(56, 'Jalan Hidup', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adg elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit.andae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.', '2022-01-29', 'doctor'),
(57, 'Ujian', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit.andae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.', '2022-01-29', 'doctor'),
(58, 'Hidup Seperti Garry', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisnsectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui. consectetur adipisicing elit. Odio recusandae quasuaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.', '2022-01-29', 'doctor'),
(59, 'Salam Dari Vrindavan', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet  adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit.andae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.', '2022-01-29', 'Anonymous'),
(60, 'Keluarga Bahagia ', 'Lorem ipsum dol dolor, sit amet consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui. consectetur adipisicing elit. Odio recusandae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.Lorem ipsum dolor, sit amet consectetur adipisicing elit.andae quasi natus hic facere quaerat perspiciatis quibusdam, quia placeat modi sed harum officiis animi earum. Molestias quis ut error qui.', '2022-01-29', 'Anonymous');

-- --------------------------------------------------------

--
-- Table structure for table `threadc`
--

CREATE TABLE `threadc` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `usertype` varchar(11) NOT NULL,
  `idthread` int(3) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `threadc`
--

INSERT INTO `threadc` (`id`, `title`, `content`, `usertype`, `idthread`, `date`) VALUES
(2, ' e at sem sit amet ultrices ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea dictumst. Aenean semper vulputate commodo. Aenean id lobortis nulla. Nullam ', 'doctor', 58, '2022-01-29'),
(3, 'Etiam volutpat viverra massa, ', 'Etiam volutpat viverra massa, eget dignissim sem dignissim ut. Nulla ullamcorper ornare ligula ut aliquam. Maecenas iaculis tincidunt mauris, sit amet pulvinar lorem vestibulum eu. Phasellus quis neque ac lacus porta molli ellentesque semper turpis id rutrum. Quisque fermentum metus vitae orci maximus fermentum. Morbi justo urna, rutrum vel porttitor in, eleifend at dolor. Aenean fermentum eros ut dapibus elementum. Phasellus feugiat in velit a fringilla.', 'doctor', 54, '2022-01-29'),
(4, ' lus feugiat in velit a fringilla.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea dictumst. Aenean semper vulputate  \r\nEtiam volutpat viverra massa, eget dignissim sem dignissim ut. Nulla ullamcorper ornare ligula ut aliquam. Maecenas iaculis tincidunt mauris, sit amet pulvinar lorem vestibulum eu. Phasellus quis neque ac  ', 'doctor', 55, '2022-01-29'),
(5, ' Proin auctor rutrum an ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate  \r\nNullam molestie sed eros id ullamcorper. Nam fringilla vel magna id mattis. Donec condimentum eget arcu nec scelerisque. Sed in ipsum ac mauris laoreet rutrum. Etiam vitae nunc nec justo bibendum varius in eu nisi. Proin e iat dolor. Suspendisse fermentum eu lorem in aliquam. Morbi porttitor, mi et scelerisque bibendum, dui tortor laoreet turpis, et scelerisque ex orci sit amet justo. Sed vel eros eu erat pulvinar iaculis. Curabitur lacinia urna lectus, non hendrerit ex accumsan mollis.', 'doctor', 57, '2022-01-29'),
(6, ' Duis rutrum, tortor in semper elementum, ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea dictumst. Aenean semper vulputate commodo. Aenean id lobortis nulla. Nullam hen  hendrerit ex accumsan mollis.sa ', 'doctor', 54, '2022-01-29'),
(7, 'neque finibus euismod id at sem. Nullam vulputate ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea dictumst. Aenean semper vulputate commodo. Aenean id lobortis nulla. Nullam hendrerit faucibus eleifend. Curabitur laoreet, lacus a dapibus blandit, felis lorem semper est, a tempus quam eros quis leo. Mauris lacinia velit vitae odio placerat malesuada. Aliquam eget effic ue at sem sit amet ultrices. Integer lacus est, rutrum a arcu ut, gravida egestas lorem. Nulla facilisi. Etiam arcu massa, posuere eget interdum quis, eleifend quis augue. Nullam pellentesque semper turpis id rutrum. Quisque fermentum metus vitae orci maximus fermentum. Morbi justo urna, rutrum vel porttitor in, eleifend at dolor. Aenean fermentum eros ut dapibus eleme', 'doctor', 56, '2022-01-29'),
(8, 'Lorem ipsum dolor sit amet, consectetur adipisci ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea dictumst. Aenean semper vulputate commodo. Aenean id lobortis nulla. Nullam hendrerit faucibus e fermentum eu lorem in aliquam. Morbi porttitor, mi et scelerisque bibendum, dui tortor laoreet turpis, et scelerisque ex orci sit amet justo. Sed vel eros eu erat pulvinar iaculis. Curabitur lacinia urna lectus, non hendrerit ex accumsan mollis.', 'doctor', 57, '2022-01-29'),
(9, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Lorem ipsum dolor sit amet, consectetur adipiscing eli r turpis id rutrum. Quisque fermentum metus vitae orci maximus fermentum. Morbi justo urna, rutrum vel porttitor in, eleifend at dolor. Aenean fermentum eros ut dapibus elementu  quis felis in, finibus ornare erat. Pellentesque laoreet, diam a vulputate auctor, tellus lacus sagittis justo, ullamcorper tempor tortor tellus volutpat ligula. Nunc et porta turpis, eget auctor do ar iaculis. Curabitur lacinia urna lectus, non hendrerit ex accumsan mollis.', 'doctor', 59, '2022-01-29'),
(10, 'Lorem ipsum dolor sit amet, conse ollis.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea dictumst. Aenean semper vulputate commodo. Aenean id lobortis nulla. Nullam hendrerit fauci gna id mattis. Donec condimentum eget arcu nec scelerisque. Sed in ipsum ac mauris laoreet rutrum. Etiam vitae nunc nec justo bibendum varius in eu nisi. Proin   vel eros eu erat pulvinar iaculis. Curabitur lacinia urna lectus, non hendrerit ex accumsan mollis.', 'doctor', 60, '2022-01-29'),
(11, 'Nullam molestie sed eros id ulla msan mollis.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea dictumst. Aenean semper vulputate commodo. Aenean id lobortis nulla. Nullam hendrerit fa ', 'doctor', 56, '2022-01-29'),
(12, 'Lorem ipsum dolor  ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac  is, sit amet pulvinar lorem vestibulum eu. Phasellus quis neque ac lacus porta mollis eu eu elit. Curabitur scelerisque at sem sit amet ultrices. Integer lacus est, rutrum a arcu ut, gravida egestas lorem. Nulla facilisi. Etiam arcu massa, posuere eget interdum quis, eleifend quis augue. Nullam pellentesque semper turpis id rutrum. Quisque fermentum me ccumsan mollis.', 'doctor', 55, '2022-01-29'),
(13, '  quam eros quis leo. Mauris lacinia velit vitae o ', '  dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac ha lorem vestibulum eu. Phasellus quis neque ac uctor rutrum ante, a lacinia purus venenatis ut. Mauris lobortis metus eu odio consequat congue. Aliquam hendrerit, lacus vitae ultricies vehicula, ligula nibh facilisis sapien, at porta est nunc eu felis. Suspe rttitor, mi et scelerisque bibendum, dui tortor laoreet turpis, et scelerisque ex orci sit amet justo. Sed vel eros eu erat pulvinar iaculis. Curabitur lacinia urna lectus, non hendrerit ex accumsan mollis.', 'doctor', 55, '2022-01-29'),
(14, 'Lorem ipsum dolor sit amet ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea d it amet justo. Sed vel eros eu erat pulvinar iaculis. Curabitur lacinia urna lectus, non hendrerit ex accumsan mollis.', 'doctor', 53, '2022-01-29'),
(15, 'accumsan mollis.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit sapien, malesuada ut odio ut, consectetur malesuada dui. Praesent et urna a neque finibus euismod id at sem. Nullam vulputate purus nec tellus porta pellentesque. Duis rutrum, tortor in semper elementum, massa diam iaculis massa, non convallis purus quam ac est. In hac habitasse platea dictumst. Aenean semper vulp ', 'doctor', 53, '2022-01-29'),
(16, 'Lorem ipsum dolor sit amet, consec ', 'itae nunc nec justo bibendum varius in eu nisi. Proin efficitur imperdiet mauris nec sagittis. Proin auctor rutrum ante, a lacinia purus venenatis ut. Mauris lobortis metus eu odio consequat congue. Aliquam hendrerit, lacus vitae ultricies vehicula, ligula nibh facilisis sapien, at porta est nunc eu felis. Sus bi porttitor, mi et scelerisque bibendum, dui tortor laoreet turpis, et scelerisque ex orci sit amet justo. Sed vel eros eu erat pulvinar iaculis. Curabitur lacinia urna lectus, non hendrerit ex accumsan mollis.', 'Anonymous', 53, '2022-01-29');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` int(20) NOT NULL,
  `usertype` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountdoctor`
--
ALTER TABLE `accountdoctor`
  ADD PRIMARY KEY (`iddoc`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`newsid`);

--
-- Indexes for table `thread`
--
ALTER TABLE `thread`
  ADD PRIMARY KEY (`idthread`);

--
-- Indexes for table `threadc`
--
ALTER TABLE `threadc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idthread` (`idthread`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountdoctor`
--
ALTER TABLE `accountdoctor`
  MODIFY `iddoc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `newsid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `thread`
--
ALTER TABLE `thread`
  MODIFY `idthread` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `threadc`
--
ALTER TABLE `threadc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `threadc`
--
ALTER TABLE `threadc`
  ADD CONSTRAINT `threadc_ibfk_1` FOREIGN KEY (`idthread`) REFERENCES `thread` (`idthread`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
